package main

import (
	"fmt"
	"log"
)

func OddEven(n int){
	if n % 2 == 0{
		fmt.Printf("%d is a even\n",n)
	}else{
		fmt.Printf("%d is not a even\n",n)
	}
}

func main(){
	for{
		var number int
		_, err := fmt.Scan(&number)
		if err != nil{
			log.Fatal(err)
		}
		OddEven(number)
	}
}