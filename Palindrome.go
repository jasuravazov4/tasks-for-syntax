package main
import "fmt"

func main() {
	for{
		var number,remainder,temp int
		reverse := 0
		fmt.Print("\nEnter any positive integer : ")
		_, err := fmt.Scan(&number)
		if err != nil {
			return
		}

		temp=number

		// For Loop used in format of While Loop
		for{
			remainder = number%10
			reverse = reverse*10 + remainder
			number /= 10

			if number==0{
				break // Break Statement used to exit from loop
			}
		}

		if temp==reverse{
			fmt.Printf("%d is a Palindrome",temp)
		}else{
			fmt.Printf("%d is not a Palindrome",temp)
		}
	}
}